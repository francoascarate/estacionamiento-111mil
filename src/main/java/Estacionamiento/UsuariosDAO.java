package Estacionamiento;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import Estacionamiento.Usuario;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author franc
 */
public class UsuariosDAO {

    private Session sesion;
    private Transaction tx;

    private void iniciaOperacion() throws HibernateException {
        sesion = HibernateUtil.getSessionFactory().openSession();
        tx = sesion.beginTransaction();
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }

    public long guardaUsuario(UsuariosDAO usuariosdao) {
        long id = 0;

        try {
            iniciaOperacion();
            id = (Long) sesion.save(usuariosdao);
            tx.commit();
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
        return id;
    }

    public void actualizaUsuario(UsuariosDAO usuariosdao) throws HibernateException {
        try {
            iniciaOperacion();
            sesion.update(usuariosdao);
            tx.commit();
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    public void eliminaUsuario(UsuariosDAO usuariosdao) throws HibernateException {
        try {
            iniciaOperacion();
            sesion.delete(usuariosdao);
            tx.commit();
        } catch (HibernateException he) {
            manejaExcepcion(he);
            throw he;
        } finally {
            sesion.close();
        }
    }

    public UsuariosDAO obtenUsuario(long idUsuario) throws HibernateException {
        UsuariosDAO usuariosdao = null;

        try {
            iniciaOperacion();
            usuariosdao = (UsuariosDAO) sesion.get(UsuariosDAO.class, idUsuario);
        } finally {
            sesion.close();
        }
        return usuariosdao;
    }

    public List<UsuariosDAO> obtenListaUsuarios() throws HibernateException {
        List<UsuariosDAO> listaUsuarios = null;

        try {
            iniciaOperacion();
            listaUsuarios = sesion.createQuery("from UsuarioDAO").list();
        } finally {
            sesion.close();
        }

        return listaUsuarios;
    }
}
