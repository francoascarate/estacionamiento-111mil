/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estacionamiento;

import java.util.Date;

/**
 *
 * @author Alumno
 */
public class Tarifa {
    public Tarifa(){
        
    }
    private int cantidadIngresosSinSaldo;
    private boolean esDeAbono;
    private Date fecha;
    private double montoIngreso;

    /**
     * @return the cantidadIngresosSinSaldo
     */
    public int getCantidadIngresosSinSaldo() {
        return cantidadIngresosSinSaldo;
    }

    /**
     * @param cantidadIngresosSinSaldo the cantidadIngresosSinSaldo to set
     */
    public void setCantidadIngresosSinSaldo(int cantidadIngresosSinSaldo) {
        this.cantidadIngresosSinSaldo = cantidadIngresosSinSaldo;
    }

    /**
     * @return the esDeAbono
     */
    public boolean isEsDeAbono() {
        return esDeAbono;
    }

    /**
     * @param esDeAbono the esDeAbono to set
     */
    public void setEsDeAbono(boolean esDeAbono) {
        this.esDeAbono = esDeAbono;
    }

    /**
     * @return the fecha
     */
    public Date getFecha() {
        return fecha;
    }

    /**
     * @param fecha the fecha to set
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * @return the montoIngreso
     */
    public double getMontoIngreso() {
        return montoIngreso;
    }

    /**
     * @param montoIngreso the montoIngreso to set
     */
    public void setMontoIngreso(double montoIngreso) {
        this.montoIngreso = montoIngreso;
    }
    
}
